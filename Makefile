EXTENSION = bgw_role_rest
MODULES = bgw_role_rest
OBJS = bgw_role_rest.so
DATA = bgw_role_rest--0.0.1.sql

PG_CONFIG=pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
export LD_LIBRARY_PATH := $(shell $(PG_CONFIG) --libdir)
include $(PGXS)

CURL = curl
LISTEN_ADDRESS = 127.0.0.1
LISTEN_PORT = 18000

installcheck:
	[ "$(shell $(CURL) -q -X HEAD -i http://$(LISTEN_ADDRESS):$(LISTEN_PORT))" = "HTTP/1.0 200 OK" ]
