#include "postgres.h"

#include "miscadmin.h"
#include "postmaster/bgworker.h"
#include "storage/ipc.h"
#include "storage/latch.h"
#include "access/xlog.h"
#include "utils/guc.h"
#include "pgstat.h"

#include <unistd.h>
#include <arpa/inet.h>

char *bin2hex(const unsigned char *input, size_t len);

PG_MODULE_MAGIC;

PG_FUNCTION_INFO_V1(bgw_role_rest_launch);

// #define IF_ERR(ARG) (if (ARG < 0) { perror(#ARG); exit(EXIT_FAILURE); })
#define MAXLINE 4096

void _PG_init(void);
void bgw_role_rest_main(Datum d) pg_attribute_noreturn();

/* flags set by signal handlers */
static volatile sig_atomic_t got_sigterm = false;

/* config */
int portnum = 18000;
char *bindaddr = NULL;

/* For error tracing when reading from socket
 */
char *bin2hex(const unsigned char *input, size_t len){
	char	*result;
	char	*hexits = "0123456789ABCDEF";
	int 	resultlenght;

	if (input == NULL || len <= 0)
		return NULL;

	resultlenght = (len*3)+1;

	result = malloc(resultlenght);
	bzero(result, resultlenght);

	for (int i=0; i<len; i++){
		result[i*3]		= hexits[input[i] >> 4];
		result[(i*3)+1]	= hexits[input[i] & 0x0F];
		result[(i*3)+2]	= ' ';
	}

	return result;
	
}


/*
 * Perform a clean shutdown on SIGTERM. To do that, just
 * set a boolean in the sig handler and then set our own
 * latch to break the main loop.
 */
static void
bgw_role_rest_sigterm(SIGNAL_ARGS)
{
	int save_errno = errno;

	got_sigterm = true;
	SetLatch(MyLatch);
	errno = save_errno;
}

void bgw_role_rest_main(Datum d)
{
	int enable = 1;
	int listensocket;
	struct sockaddr_in addr;

	// uint8_t		recvline[MAXLINE+1];
	

	pqsignal(SIGTERM, bgw_role_rest_sigterm);

	BackgroundWorkerUnblockSignals();

	/* Setup our listening socket */
	listensocket = socket(AF_INET, SOCK_STREAM, 0);
	if (listensocket == -1)
		ereport(ERROR,
				(errmsg("bgw_role_rest: could not create socket: %m")));

	if (setsockopt(listensocket, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) != 0)
		ereport(ERROR,
				(errmsg("bgw_role_rest: could not set socket option: %m")));

	if (!pg_set_noblock(listensocket))
		ereport(ERROR,
				(errmsg("bgw_role_rest: could not set non blocking socket: %m")));

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
		addr.sin_port = htons(portnum);

	if (bindaddr == NULL || strlen(bindaddr) == 0)
		addr.sin_addr.s_addr = INADDR_ANY;
	else
	{
		if (inet_aton(bindaddr, &addr.sin_addr) == 0)
			ereport(ERROR,
					(errmsg("bgw_role_rest: could not translate IP address '%s'",
							bindaddr)));
	}

	if (bind(listensocket, &addr, sizeof(addr)) != 0)
		ereport(ERROR,
				(errmsg("bgw_role_rest: could not bind socket: %m")));

	if (listen(listensocket, 5) != 0)
		ereport(ERROR,
				(errmsg("bgw_role_rest: could not listen on socket: %m")));

	/*
	 * Loop forever looking for new connections. Terminate on SIGTERM,
	 * which is sent by the postmaster when it wants us to shut down.
	 * XXX: we don't currently support changing the port at runtime.
	 */
	while (!got_sigterm)
	{
		int rc;

		rc = WaitLatchOrSocket(MyLatch,
							   WL_LATCH_SET | WL_POSTMASTER_DEATH | WL_SOCKET_READABLE,
							   listensocket,
							   -1
#if PG_VERSION_NUM >= 100000
							   /* 10.0 introduced PG_WAIT_EXTENSION */
							   ,PG_WAIT_EXTENSION);
#else
							   );
#endif
		ResetLatch(MyLatch);

		if (rc & WL_POSTMASTER_DEATH)
			proc_exit(1);
		else if (rc & WL_SOCKET_READABLE)
		{
			char status_str[MAXLINE+1];
			// uint8_t		status_str[MAXLINE+1];
			socklen_t addrsize = sizeof(addr);
			int worksock = accept4(listensocket, &addr, &addrsize, SOCK_NONBLOCK);
			if (worksock == -1)
			{
				ereport(LOG,
						(errmsg("bgw_role_rest: could not accept socket: %m")));
				continue;
			}

			// Need to iterate to parse the request
			// memset(recvline, 0, MAXLINE);
			// fprintf(stdout,"\n%s\n\n%s", bin2hex(recvline, n),recvline);
			
			if (RecoveryInProgress()){
				snprintf((char *)status_str, sizeof(status_str), "HTTP/1.0 503 Service Unavailable\r\n\r\n");
			} else {
				snprintf((char *)status_str, sizeof(status_str), "HTTP/1.0 200 OK\r\n\r\n");	
			}

			if (write(worksock, status_str, strlen(status_str)) != strlen(status_str))
			{
				ereport(LOG,
						(errmsg("bgw_role_rest: could not write %s: %m",
								status_str)));
				close(worksock);
				continue;
			}

			if (close(worksock) != 0)
			{
				ereport(LOG,
						(errmsg("bgw_role_rest: could not close working socket: %m")));
				continue;
			}
		}
	}

	ereport(LOG,
			(errmsg("bgw_role_rest: shutting down")));

	close(listensocket);

	proc_exit(0);
}


/*
 * Initialization entrypoint
 */
void _PG_init(void)
{
	BackgroundWorker worker;

	/*
	 * Define our GUCs so we can get the configuration values.
	 */
	DefineCustomIntVariable("bgw_role_rest.port",
							"TCP port to bind to",
							NULL,
							&portnum,
							18000,
							1025,
							65535,
							PGC_POSTMASTER,
							0,
							NULL,
							NULL,
							NULL);

	DefineCustomStringVariable("bgw_role_rest.bind",
							   "IP address to bind to",
							   NULL,
							   &bindaddr,
							   "",
							   PGC_POSTMASTER,
							   0,
							   NULL,
							   NULL,
							   NULL);

	if (!process_shared_preload_libraries_in_progress)
		return;

	/*
	 * Set up our bgworker
	 */
	MemSet(&worker, 0, sizeof(worker));

	worker.bgw_flags = BGWORKER_SHMEM_ACCESS;
	worker.bgw_start_time = BgWorkerStart_PostmasterStart;
	worker.bgw_restart_time = 30; /* Restart after 30 seconds -- just
									 so we don't end up in a hard loop
									 if something fails */
	sprintf(worker.bgw_library_name, "bgw_role_rest");
	sprintf(worker.bgw_function_name, "bgw_role_rest_main");
	worker.bgw_notify_pid = 0;
	snprintf(worker.bgw_name, BGW_MAXLEN, "bgw_role_rest");

	RegisterBackgroundWorker(&worker);
}
