# [EXPERIMENTAL] bgw_role_rest


This extension is EXPERIMENTAL, not ready for any kind of serious usage. 
It's based on the [bgw_replstatus](https://github.com/mhagander/bgw_replstatus)
extension.


`bgw_role_rest` is a tiny background worker to cheaply report the
replication status of a node, responding with the HTTP headers for 
load balancing REST-compatible entrypoints.

This is extremelly in early stages, it can be extended using [ulfius](https://github.com/babelouest/ulfius)
as an API framework.


When installed, a background worker will be started that listens on a
defined TCP port (configured `bgw_role_rest.port`). Any connection to
this port will get an HTTP response, with 200 or 503 accordingly to the
state of the node.


## Installing

Build and install is done using PGXS. As long as `pg_config` is
available in the path, build and install using:

```
$ make
$ make install
```

Once the binary is installed, append `bgw_role_test` at
`shared_preload_libraries` in postgresql.conf:

```
shared_preload_libraries = '..., bgw_role_rest'
```

```sql
CREATE EXTENSION bgw_role_rest
```

## Configuration


By default, the background worker will listen to port 18000 on a
wild card IP address. There is *no* verification of the source done, so
protect the port with a proper host firewall!!!

To change the port, set the value of `bgw_role_rest.port` to another
value. Any TCP port above 1024 will work (but don't pick the same one
as PostgreSQL itself...).

To change the socket to bind to a specific IP address, set
`bgw_role_rest.bind` to an IP address, which will cause the
background worker to bind to this IP on the defined port.

There is no support for multiple ports or multiple IP addresses.

## Example usage


```
$ curl -q -X HEAD -i http://localhost:18000
```

Since the text coming back is easily identifiable, it's easy enough to
integrate with a load balancer such as haproxy. This example haproxy
configuration will show how to ensure that haproxy is connected to the
master node of the cluster, and automatically switches over to the
backup node if the master goes down and the backup is
promoted. Multiple backups can be used.

[WIP]
```
frontend test
	bind 127.0.0.1:5999
	default_backend pgcluster

backend pgcluster
	mode http
	option httpchk
	http-check send meth GET  uri /
	server s1 127.0.0.1:18000 check port 5432
	server s2 127.0.0.1:18000 check port 5432 backup
	server s3 127.0.0.1:18000 check port 5432 backup
```
